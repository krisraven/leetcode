package main

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func Test_twoSum(t *testing.T) {
	assert.Equal(t, twoSum([]int{2, 7, 11, 15}, 9), []int{1, 2})
	assert.Equal(t, twoSum([]int{2,3,4}, 6), []int{1, 3})
	assert.Equal(t, twoSum([]int{-1,0}, -1), []int{1, 2})
	assert.Equal(t, twoSum([]int{0,0,3,4}, 0), []int{1, 2})
	assert.Equal(t, twoSum([]int{1,2,3,4,4,9,56,90}, 8), []int{4, 5})


}

func Test_twoSumPointer(t *testing.T) {
	assert.Equal(t, twoSumPointer([]int{2, 7, 11, 15}, 9), []int{1, 2})
	assert.Equal(t, twoSumPointer([]int{2,3,4}, 6), []int{1, 3})
	assert.Equal(t, twoSumPointer([]int{-1,0}, -1), []int{1, 2})
	assert.Equal(t, twoSumPointer([]int{0,0,3,4}, 0), []int{1, 2})
	assert.Equal(t, twoSumPointer([]int{1,2,3,4,4,9,56,90}, 8), []int{4, 5})

}