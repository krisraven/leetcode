package main

import (
	"sort"
)

func toKeys(valMap map[int]bool) []int {
	result := []int{}
	for k, v := range valMap {
		if v {
			result = append(result, k)
		}
	}
	sort.Ints(result)
	return result
}

func twoSum(numbers []int, target int) []int {
	result := make(map[int]bool, len(numbers))
	for i := 0; i < len(numbers); i++ {
		for j := 1; j < len(numbers); j++ {
			if numbers[i]+numbers[j] == target && i!=j {
				result[i+1] = true
				result[j+1] = true
			}
		}
	}
	return toKeys(result)
}

func twoSumPointer(numbers []int, target int) []int   {
	if numbers == nil || len(numbers) == 0 {
		return []int{}
	}
	left:= 0
	right:= len(numbers)-1
	for left <right {
		sum:= numbers[left] + numbers[right]
		if sum > target{
			right --
		}else if sum <target{
			left ++
		}else {
			return []int{left+1,right+1}
		}

	}
	return []int{}
}
