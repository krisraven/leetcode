package main

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func Test_reverse(t *testing.T) {
	assert.Equal(t, reverse(542), 245)
	assert.Equal(t, reverse(-12345), -54321)
	assert.Equal(t, reverse(1534236469), 0)
	assert.Equal(t, reverse(-2147483412), -2143847412)
	assert.Equal(t, reverse(-2147483648), 0)
}

func Test_reverseString(t *testing.T) {
	assert.Equal(t, reverseUsingString(542), 245)
	assert.Equal(t, reverseUsingString(-12345), -54321)
	assert.Equal(t, reverseUsingString(1534236469), 0)
	assert.Equal(t, reverseUsingString(-2147483412), -2143847412)
	assert.Equal(t, reverseUsingString(-2147483648), 0)
}
