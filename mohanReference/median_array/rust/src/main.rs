struct Solution {}

impl Solution {
    pub fn get_total(nums1: &Vec<i32>, nums2: &Vec<i32>) -> i32 {
        (nums1.len() + nums2.len()) as i32
    }

    fn compare(a: i32, b: i32) -> Vec<i32> {
        if a == b {
            return vec![a, b];
        } else if a < b {
            return vec![a];
        } else {
            return vec![b];
        }
    }

    fn find_elems(val1: &Option<&i32>, val2: &Option<&i32>) -> Vec<i32> {
        if val1.is_some() && val2.is_some() {
            let a = val1.unwrap().clone();
            let b = val2.unwrap().clone();
            return Solution::compare(a, b);
        } else if val1.is_some() {
            return vec![*val1.unwrap()];
        } else if val2.is_some() {
            return vec![*val2.unwrap()];
        } else {
            return vec![];
        }
    }

    fn insert(result_vec: &mut Vec<i32>, vals: &Vec<i32>) {
        for v in vals.iter() {
            result_vec.push(v.clone());
        }
    }

    fn has(vec: &Vec<i32>, i: usize, val_vec: &Vec<i32>) -> bool {
        if vec.get(i).is_none() {
            return false;
        }
        let val = vec.get(i).unwrap().clone();
        return val_vec.contains(&val);
    }

    fn find_median(vec: &Vec<i32>) -> f64 {
        if vec.len() % 2 == 0 {
            let mid1 = vec.len() / 2;
            let mid2 = mid1 - 1;
            let a = vec[mid1] as f64;
            let b = vec[mid2] as f64;
            return (a + b) / 2.0;
        } else {
            let midp = vec.len() / 2;
            return vec[midp] as f64;
        }
    }

    pub fn find_median_sorted_arrays(nums1: Vec<i32>, nums2: Vec<i32>) -> f64 {
        let total_arr = Solution::get_total(&nums1, &nums2);
        let mut merged_array: Vec<i32> = Vec::<i32>::with_capacity(total_arr as usize);
        let mut i = 0;
        let mut j = 0;
        let mut k = 0;
        while k <= total_arr {
            let a = nums1.get(i);
            let b = nums2.get(j);
            let smaller = Solution::find_elems(&a, &b);
            Solution::insert(&mut merged_array, &smaller);
            if Solution::has(&nums1, i, &smaller) {
                i = i + 1;
            }
            if Solution::has(&nums2, j, &smaller) {
                j = j + 1;
            }
            k = k + 1;
        }
        return Solution::find_median(&merged_array);
    }
}

fn main() {
    println!("{}", Solution::find_median_sorted_arrays(vec![1], vec![]));
    println!(
        "{}",
        Solution::find_median_sorted_arrays(vec![1, 2], vec![4, 7, 9000])
    );
    println!(
        "{}",
        Solution::find_median_sorted_arrays(vec![1, 2], vec![4, 7])
    );
    println!(
        "{}",
        Solution::find_median_sorted_arrays(vec![1, 2], vec![3, 4])
    );
    println!(
        "{}",
        Solution::find_median_sorted_arrays(vec![9, 10], vec![3, 4])
    );
    let a = vec![1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 4, 4];
    let b = vec![1, 3, 4, 4, 4, 4, 4, 4, 4, 4, 4];
    println!("{}", Solution::find_median_sorted_arrays(a, b));
}
