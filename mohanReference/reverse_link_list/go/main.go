package main

type ListNode struct {
	Val  int
	Next *ListNode
}

func reverseList(head *ListNode) *ListNode {
	if head == nil {
		return nil
	}
	reversedList := &ListNode{Val:head.Val,
		Next: nil}
	current:= head.Next
	for current != nil {
		n:= &ListNode{Val:current.Val,Next:reversedList}
		reversedList = n
		current=current.Next
	}
	return reversedList
}