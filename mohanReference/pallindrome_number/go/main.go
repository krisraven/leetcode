package main



func reverse(x int) int   {
	reverse :=0
	for x != 0 {
		rem := x %10
		x = x/10
		reverse = reverse *10 + rem
	}
	return reverse
}
func isPalindrome(x int) bool {
	if x <0 {
		return  false
	}
	if x <=9 {
		return  true
	}
	return x == reverse(x)
}