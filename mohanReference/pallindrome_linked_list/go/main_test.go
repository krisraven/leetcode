package main

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func Test_isPalindromeTwoPass(t *testing.T) {
	l := &ListNode{1, &ListNode{2, &ListNode{1, nil}}}
	l1 := &ListNode{1, &ListNode{2, nil}}
	l2 := &ListNode{1, &ListNode{0, &ListNode{3, &ListNode{4,
		&ListNode{0, &ListNode{1, nil}}}}}}

	assert.Equal(t, isPalindromeTwoPass(l), true)
	assert.Equal(t, isPalindromeTwoPass(l1), false)
	assert.Equal(t, isPalindromeTwoPass(l2), false)
}

func Test_isPalindrome(t *testing.T) {
	l := &ListNode{1, &ListNode{2, &ListNode{1, nil}}}
	l1 := &ListNode{1, &ListNode{2, nil}}
	l2 := &ListNode{1, &ListNode{0, &ListNode{3, &ListNode{4,
		&ListNode{0, &ListNode{1, nil}}}}}}

	assert.Equal(t, isPalindrome(l), true)
	assert.Equal(t, isPalindrome(l1), false)
	assert.Equal(t, isPalindrome(l2), false)
}
