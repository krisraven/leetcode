use std::fmt::Display;

#[derive(Debug)]
pub struct Stack<T: Display> {
    vec: Vec<T>,
}

impl<T: Display> Stack<T> {
    pub fn new() -> Stack<T> {
        Stack { vec: vec![] }
    }

    pub fn push(&mut self, val: T) {
        self.vec.push(val)
    }
    pub fn pop(&mut self) -> Option<T> {
        if self.vec.is_empty() {
            return None;
        }
        Some(self.vec.remove(self.vec.len() - 1))
    }

    pub fn is_empty(&self) -> bool {
        self.vec.is_empty()
    }
    pub fn print(&self) {
        for v in self.vec.iter() {
            println!("{}", v);
        }
    }
}
