mod binarytree;
mod queue;
mod stack;

use binarytree::Node;

fn main() {
    let mut n = Node::new(10);
    let vec = vec![5, 15, 8, 16, 20];
    for v in vec {
        n.insert(v);
    }
    Node::print(&n);
    println!("--------------");
    Node::bfs(&n);
    println!("*********************************");
    Node::dfs(&n);
}
