package btree

import (
	"fmt"

	"leetcode/ds/queue"
	"leetcode/ds/stack"
)

type NodeInt struct {
	Val   int
	left  *NodeInt
	right *NodeInt
}

func New(Val int) *NodeInt {
	return &NodeInt{
		Val: Val,
	}

}

func (n *NodeInt) insertLeft(Val int) {
	if n.left == nil {
		n.left = New(Val)
		return
	}
	n.left.Insert(Val)
}

func (n *NodeInt) insertRight(Val int) {
	if n.right == nil {
		n.right = New(Val)
		return
	}
	n.right.Insert(Val)
}

func (n *NodeInt) Insert(Val int) {
	if Val < n.Val {
		n.insertLeft(Val)
	} else {
		n.insertRight(Val)
	}

}

func (n *NodeInt) Find(Val int) bool {
	if n == nil {
		return false
	}
	if n.Val == Val {
		return true
	}

	if Val < n.Val && n.left != nil {
		return n.left.Find(Val)
	}

	if Val > n.Val && n.right != nil {
		return n.right.Find(Val)
	}
	return false
}

func (n *NodeInt) print(nodeType string) {
	if n.left != nil {
		n.left.print("left")
	}
	fmt.Println(fmt.Sprintf("%s:%d", nodeType, n.Val))
	if n.right != nil {
		n.right.print("right")
	}
}
func (n *NodeInt) Print() {
	n.print("root")
}

type NodeIntFunc func(node *NodeInt)

func (n *NodeInt) Bfs(f NodeIntFunc) {
	if n == nil {
		return
	}
	q := queue.NewQueueInterface()
	q.Enqueue(n)
	for !q.IsEmpty() {
		elem := q.Dequeue().(*NodeInt)
		f(elem)
		if elem.left != nil {
			q.Enqueue(elem.left)

		}
		if elem.right != nil {
			q.Enqueue(elem.right)
		}
	}
}

func (n *NodeInt) dfs(f NodeIntFunc) {
	if n == nil {
		return
	}
	st := stack.NewStackInterface()
	st.Push(n)
	for !st.IsEmpty() {
		elem := st.Pop().(*NodeInt)
		f(elem)
		if elem.left != nil {
			st.Push(elem.left)

		}
		if elem.right != nil {
			st.Push(elem.right)
		}
	}
}

func max(a int, b int) int {
	if a > b {
		return a
	}
	return b
}

func min(a int, b int) int {
	if a < b {
		return a
	}
	return b
}
func Height(n *NodeInt) int {
	if n == nil {
		return 0
	}
	leftHeight := 1 + Height(n.left)
	rightHeight := 1 + Height(n.right)
	return max(leftHeight, rightHeight)
}

func MinHeight(n *NodeInt) int {
	if n == nil {
		return 0
	}
	leftHeight := 1 + Height(n.left)
	rightHeight := 1 + Height(n.right)
	return min(leftHeight, rightHeight)
}



