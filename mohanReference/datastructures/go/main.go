package main

import (
	"fmt"
	"leetcode/ds/btree"
	"leetcode/ds/graph"
)

func testBtree() {
	testVal := []int{2, 4, 1, 3, 6}
	testNode := btree.New(5)
	for _, val := range testVal {
		testNode.Insert(val)
	}
	testNode.Print()
	fmt.Println("---------------------")
	testNode.Bfs(func(node *btree.NodeInt) {
		fmt.Println(node.Val)
	})
	fmt.Println("++++**************************++++")
	fmt.Println(btree.Height(testNode))
	fmt.Println("min height :", btree.MinHeight(testNode))

}

func testGraph()  {
	g := graph.NewGraphInt()
	g.AddVertex("A")
	g.AddVertex("B")
	g.AddVertex("C")
	g.AddVertex("D")
	g.AddVertex("E")
	g.AddVertex("F")
	g.AddEdge(0, 1)
	g.AddEdge(0, 3)
	g.AddEdge(3, 5)
	g.AddEdge(0, 4)

	g.PrintEdges()
	fmt.Println("-------------------------")
	g.Dfs(func(n interface{}) {
		fmt.Println(n.(string))
	})
	fmt.Println("End Dfs-------------------------")
	g.Bfs(func(n interface{}) {
		fmt.Println(n.(string))
	})
	fmt.Println("End Bfs-------------------------")
}

func main() {
	testGraph()
}
