package graph

import (
	"fmt"

	"leetcode/ds/queue"
	"leetcode/ds/stack"
)

//Adj matrix
type Graph struct {
	vertex []interface{}
	edges  [][]int
}

func (g *Graph) resizeEdges() {
	edges := make([][]int, len(g.vertex))
	for i := range edges {
		edges[i] = make([]int, len(g.vertex))
	}
	g.edges = edges
}
func NewGraphInt() *Graph {
	return &Graph{}
}

func (g *Graph) AddVertex(i interface{}) {
	g.vertex = append(g.vertex, i)
	g.resizeEdges()
}

func (g *Graph) PrintVertex() {
	for i := range g.vertex {
		fmt.Println(i)
	}
}

func (g *Graph) AddEdge(start int, end int) {
	if start < 0 || end < 0 || start >= len(g.vertex) || end >= len(g.vertex) {
		return
	}
	g.edges[start][end] = 1
	g.edges[end][start] = 1

}

func (g *Graph) PrintEdges() {
	for row := range g.edges {
		for col := range g.edges[row] {
			fmt.Printf("[%d][%d]=%v\n", row, col, g.edges[row][col])
		}
	}
}

type GraphFunc func(n interface{})

func (g *Graph) findAdjacentNodeIndex(visited []bool, currentVertex int) int {
	for i := 0; i < len(g.vertex); i++ {
		if g.edges[currentVertex][i] == 1 && visited[i] == false {
			return i
		}
	}
	return -1
}

func (g *Graph) Dfs(f GraphFunc) {
	if g.vertex == nil || len(g.vertex) == 0 {
		return
	}
	visited := make([]bool, len(g.vertex))
	s := stack.NewStackInterface()
	s.Push(0)
	visited[0] = true
	f(g.vertex[0])
	for !s.IsEmpty() {
		adjacentIndex := g.findAdjacentNodeIndex(visited, s.Peek().(int))
		if adjacentIndex == -1 {
			s.Pop()
		} else {
			s.Push(adjacentIndex)
			visited[adjacentIndex] = true
			f(g.vertex[adjacentIndex])
		}
	}
}

func (g *Graph) Bfs(f GraphFunc) {
	if g.vertex == nil || len(g.vertex) == 0 {
		return
	}
	visited := make([]bool, len(g.vertex))
	q := queue.NewQueueInterface()
	q.Enqueue(0)
	visited[0] = true
	f(g.vertex[0])
	for !q.IsEmpty() {
		adjacentIndex := g.findAdjacentNodeIndex(visited, q.Peek().(int))
		if adjacentIndex == -1 {
			q.Dequeue()
		} else {
			q.Enqueue(adjacentIndex)
			visited[adjacentIndex] = true
			f(g.vertex[adjacentIndex])
		}
	}
}
