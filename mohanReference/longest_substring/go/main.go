package main

func getNonRepeatCount(s string) int {
	count := 0
	found := make(map[string]bool, len(s))
	for i := 0; i < len(s); i++ {
		if found[string(s[i])] {
			return count
		}
		found[string(s[i])] = true
		count++
	}
	return count
}
func max(a int, b int) int {
	if a > b {
		return a
	} else {
		return b
	}
}

func lengthOfLongestSubstring(s string) int {
	if s == "" {
		return 0
	}
	maxCount := 0
	for i := 0; i < len(s); i++ {
		count := getNonRepeatCount(s[i:])
		maxCount = max(maxCount, count)
	}
	return maxCount
}

func lengthOfLongestSubstringWithSlidingWindow(s string) int {
	charMap := make(map[string]bool)
	maxCount := 0
	windowStart, windowEnd := 0, 0
	for windowStart < len(s) && windowEnd < len(s) {
		if !charMap[string(s[windowEnd])] {
			charMap[string(s[windowEnd])] = true
			maxCount = max(maxCount, windowEnd-windowStart+1)
			windowEnd++
		} else {
			delete(charMap, string(s[windowStart]))
			windowStart++
		}
	}
	return maxCount
}
