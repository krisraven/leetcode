package main

import (
	"fmt"
	"io/ioutil"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

func Test_lengthOfLongestSubstring(t *testing.T) {
	assert.Equal(t, lengthOfLongestSubstring("abcabcbb"), 3)
	assert.Equal(t, lengthOfLongestSubstring("bbbbbbb"), 1)
	assert.Equal(t, lengthOfLongestSubstring("pwwkew"), 3)
	assert.Equal(t, lengthOfLongestSubstring(""), 0)

}

func TestLongSubstring(t *testing.T) {
	data, err := ioutil.ReadFile("test_data/test.txt")
	if err != nil {
		t.Fatalf("cannot read file because :%v", err)
	}
	start := time.Now()
	brutalCount := lengthOfLongestSubstring(string(data))
	end := time.Now()
	t.Log(fmt.Sprintf("time taken for brute force :%f", end.Sub(start).Seconds()))
	start = time.Now()
	slidingWindowCount := lengthOfLongestSubstringWithSlidingWindow(string(data))
	end = time.Now()
	t.Log(fmt.Sprintf("time taken for slide window :%f", end.Sub(start).Seconds()))
	assert.Equal(t, brutalCount, slidingWindowCount)
	assert.Equal(t, 95, slidingWindowCount)
}

func Test_lengthOfLongestSubstringWithSlidingWindow(t *testing.T) {
	assert.Equal(t, lengthOfLongestSubstringWithSlidingWindow("abcabcbb"), 3)
	assert.Equal(t, lengthOfLongestSubstringWithSlidingWindow("bbbbbbb"), 1)
	assert.Equal(t, lengthOfLongestSubstringWithSlidingWindow("pwwkew"), 3)
	assert.Equal(t, lengthOfLongestSubstringWithSlidingWindow(""), 0)
	assert.Equal(t, lengthOfLongestSubstringWithSlidingWindow(
		" "), 1)
	assert.Equal(t, lengthOfLongestSubstringWithSlidingWindow(
		"au"), 2)

	assert.Equal(t, lengthOfLongestSubstringWithSlidingWindow(
		"a"), 1)

}
