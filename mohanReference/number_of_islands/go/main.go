package main

import (
	"fmt"

	"leetcode/number_of_islands/v1/stack"
)

type NodeInfo struct {
	row int
	col int
}

func key(row int, col int) string {
	return fmt.Sprintf("%d_%d", row, col)
}

func currentIslandCount(grid [][]byte, row int, col int) int {
	s := stack.NewStackInterface()
	s.Push(&NodeInfo{
		row,
		col,
	})
	visited := make(map[string]bool, len(grid))
	for !s.IsEmpty() {
		node := s.Pop().(*NodeInfo)
		if node.row < 0 || node.row >= len(grid) ||
			node.col < 0 || node.col >= len(grid[row]) ||
			string(grid[node.row][node.col]) == "0" ||
			visited[key(node.row, node.col)] {
			continue
		} else {
			grid[node.row][node.col] = "-1"[0]
			visited[key(node.row, node.col)] = true
			s.Push(&NodeInfo{node.row - 1, node.col}) //top
			s.Push(&NodeInfo{node.row + 1, node.col}) //bottom
			s.Push(&NodeInfo{node.row, node.col - 1}) //left
			s.Push(&NodeInfo{node.row, node.col + 1}) //right
		}

	}

	return 1
}

func numIslands(grid [][]byte) int {
	if grid == nil || len(grid) == 0 {
		return 0
	}
	islandCount := 0
	for row := range grid {
		for col := range grid[row] {
			if string(grid[row][col]) == "1" {

				islandCount = islandCount + currentIslandCount(grid, row, col)

			}
		}
	}
	return islandCount
}

func main() {

	// do nothing
}
