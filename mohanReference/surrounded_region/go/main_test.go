package main

import (
	"testing"
	"github.com/stretchr/testify/assert"
)

// assumes the given array is of 0's and 1's
func toByte2dArr(stringArr [][]string) [][]byte {
	var byteArr [][]byte
	for _, arr1 := range stringArr {
		var elems []byte
		for _, elem := range arr1 {
			elems = append(elems, []byte(elem)[0])
		}
		byteArr = append(byteArr, elems)
	}
	return byteArr
}
func toStringArr(byteArr [][]byte) [][]string {
	var stringArr [][]string
	for _, arr1 := range byteArr {
		var elems []string
		for _, elem := range arr1 {
			elems = append(elems, string(elem))
		}
		stringArr = append(stringArr, elems)
	}
	return stringArr
}

func Test_solve(t *testing.T) {
	grid1 := [][]string{
		{"X", "X", "X", "X"},
		{"X", "O", "O", "X"},
		{"X", "X", "O", "X"},
		{"X", "O", "X", "X"},
	}
	expected := [][]string{
		{"X", "X", "X", "X"},
		{"X", "X", "X", "X"},
		{"X", "X", "X", "X"},
		{"X", "O", "X", "X"},
	}
	actual := _solve(toByte2dArr(grid1))
	assert.Equal(t, len(expected), len(actual))
	assert.Equal(t, expected, toStringArr(actual))
}

func Test_Solve2(t *testing.T) {
	grid1 := [][]string{
		{"X", "O", "X"},
		{"X", "O", "X"},
		{"X", "O", "X"}}
	expected := [][]string{
		{"X", "O", "X"},
		{"X", "O", "X"},
		{"X", "O", "X"}}
	actual := _solve(toByte2dArr(grid1))
	assert.Equal(t, len(expected), len(actual))
	assert.Equal(t, expected, toStringArr(actual))
}

func Test_Solve3(t *testing.T) {
	grid1 := [][]string{
		{"O", "X", "X", "O", "X"},
		{"X", "O", "O", "X", "O"},
		{"X", "O", "X", "O", "X"},
		{"O", "X", "O", "O", "O"},
		{"X", "X", "O", "X", "O"}}
	expected := [][]string{
		{"O", "X", "X", "O", "X"},
		{"X", "X", "X", "X", "O"},
		{"X", "X", "X", "O", "X"},
		{"O", "X", "O", "O", "O"},
		{"X", "X", "O", "X", "O"}}

	actual := _solve(toByte2dArr(grid1))
	assert.Equal(t, len(expected), len(actual))
	assert.Equal(t, expected, toStringArr(actual))
}
